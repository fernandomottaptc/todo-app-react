module.exports = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE, PATCH')
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Requested-With, Accept')
    next()
}