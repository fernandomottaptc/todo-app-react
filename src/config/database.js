const mongoose = require('mongoose')
// mongoose.Promise = global.Promise

module.exports = mongoose.connect(
    'mongodb://root:2111852111@192.168.0.249:27017/todo?authSource=admin',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        socketTimeoutMS: 30000, // 1500
        connectTimeoutMS: 30000,
        // reconnectTries: 400, // Never stop trying to reconnect
        // reconnectInterval: 500, // Reconnect every 500ms
        keepAlive: true,
        keepAliveInitialDelay: 30000
    });