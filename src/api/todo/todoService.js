const Todo = require('./todo.js')

Todo.methods(['get', 'post', 'put', 'delete'])
Todo.updateOptions({new: true, runValidation: true})

module.exports = Todo